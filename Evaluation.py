# -*- coding: utf-8 -*-
'''A utility class for evaluating the performance of a policy in multi-armed bandit problems.'''

__author__ = "Olivier Cappé, Aurélien Garivier"
__version__ = "$Revision: 1.10 $"


import numpy as np
#from translate.misc.progressbar import ProgressBar

class Evaluation:
  
    def __init__(self, env, pol, nbRepetitions, horizon, tsav=[]):
        if len(tsav)>0:
            self.tsav = tsav
        else:
            self.tsav = np.arange(horizon)
        self.env = env
        self.pol = pol
        self.nbRepetitions = nbRepetitions
        self.horizon = horizon
        self.nbArms = env.nbArms
        self.nbPulls = np.zeros((self.nbRepetitions, self.nbArms))
        self.cumReward = np.zeros((self.nbRepetitions, len(tsav)))
        self.cumChoices = [[0 for y in range(self.horizon)] for y in range(nbRepetitions)]
        self.probabilities = [[[0. for y in range(env.nbArms)] for x in range(len(tsav))] for i in range(self.nbRepetitions)]
        self.winnerPulls = [0 for y in range(env.nbArms)]
        self.winnerReward = [0 for y in range(env.nbArms)]
        
        # progress = ProgressBar()
        for k in range(nbRepetitions): # progress(range(nbRepetitions)):
            result = env.play(pol, horizon)
            self.nbPulls[k,:] = result.getNbPulls()
            self.cumReward[k,:] = np.cumsum(result.rewards)[tsav]
            self.cumChoices[k] = result.choices
            self.probabilities[k] = result.getProbability()
        # progress.finish()
        
    def meanNbDraws(self):
        result = np.mean(self.nbPulls ,0)
        for k in self.nbPulls: 
                self.winnerPulls[np.argmax(k)] += 1
        return result
     
    def meanReward(self):
        return sum(self.cumReward[:,-1])/len(self.cumReward[:,-1])
    
    def meanRewardForArm(self):
        result = [0. for i in range(self.nbArms)]
        for k in range(len(self.cumReward)):
            winnerRewardPart = [0 for y in range(self.nbArms)]
            for rep in range(len(self.cumReward[k])):
                if rep == 0:
                    result[self.cumChoices[k][rep]] += self.cumReward[k][rep]     
                else:
                    result[self.cumChoices[k][rep]] += self.cumReward[k][rep] - self.cumReward[k][rep-1]
                winnerRewardPart[self.cumChoices[k][rep]] += 1  
            self.winnerReward[np.argmax(winnerRewardPart)] += 1
        for res in range(len(result)):
            result[res] = round((result[res]/self.nbRepetitions),2)       
        return result

    def meanProbabilities(self):
        result = [[0. for y in range(self.nbArms)] for x in range(self.horizon)]
        listChoice = self.cumChoices[0]
        last = [0,0]
        for i in range(self.horizon):
            for arm in range(self.nbArms):
                ell = i+1
                elm = listChoice[:ell]
                cnt = listChoice[:ell].count(arm)
                result[i][arm] = (cnt)/float(ell)
                last[arm] =  cnt
        return result
    
    def meanRegret(self):
        return (1+self.tsav)*max([arm.expectation for arm in self.env.arms]) - np.mean(self.cumReward, 0)

    
