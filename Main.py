# -*- coding: utf-8 -*-
'''Demonstration file for the pyBandits package'''

__author__ = "Olivier Cappé, Aurélien Garivier, Emilie Kaufmann"
__version__ = "$Revision: 1.6 $"


from environment.MAB import MAB
from arm.Bernoulli import Bernoulli
from arm.Binomial import Binomial
from arm.Poisson import Poisson
from arm.Exponential import Exponential
from arm.SocialSimulator import BranchProcess
from policy.UCB import UCB
from numpy import *
from matplotlib.pyplot import *
from util.WebArrivalsSimulator import WebArrivalsSimulator

from policy.UCBV import UCBV
from policy.klUCB import klUCB
from policy.KLempUCB import KLempUCB
from policy.BayesUCB import BayesUCB
from Evaluation import *
from kullback import *
import copy

colors = ['blue', 'green', 'red', 'cyan', 'magenta', 'black']
graphic = 'yes'

# Experient Repetitions
nbRep = 10

# Number of pulls
wa = WebArrivalsSimulator(2,1000)
webArrivals = wa.getWebArrivals()
horizon = len(webArrivals)

print 'WebArrivals: ',len(webArrivals)

PoissonP = Poisson(0.9)
BernoulliP = Bernoulli(1)
PoissonP2 = Poisson(0.1)
BernoulliP2 = Bernoulli(1)
population = 1000
arrivalRate = 0.5
blacklist=[]
blacklist2 = []
blacklist3 = []
blacklist4 = []


env = [MAB([BranchProcess(BernoulliP,PoissonP,webArrivals,population,arrivalRate,blacklist),
            BranchProcess(BernoulliP2,PoissonP2,webArrivals,population,arrivalRate,blacklist)]),
       MAB([BranchProcess(BernoulliP,PoissonP,webArrivals,population,arrivalRate,blacklist2),
            BranchProcess(BernoulliP2,PoissonP2,webArrivals,population,arrivalRate,blacklist2)]),
       MAB([BranchProcess(BernoulliP,PoissonP,webArrivals,population,arrivalRate,blacklist3),
            BranchProcess(BernoulliP2,PoissonP2,webArrivals,population,arrivalRate,blacklist3)]),
       MAB([BranchProcess(BernoulliP,PoissonP,webArrivals,population,arrivalRate,blacklist4),
            BranchProcess(BernoulliP2,PoissonP2,webArrivals,population,arrivalRate,blacklist4)])]

policies = [UCB(env[3].nbArms),UCBV(env[0].nbArms),klUCB(env[1].nbArms),KLempUCB(env[2].nbArms)]


tsav = int_(linspace(100,horizon-1,horizon))

if graphic == 'yes':
    figure(1)

k=0
for policy in policies:
    print policy
     
    ev = Evaluation(env[k], policy, nbRep, horizon, tsav)
    print 'Mean Reward: '
    print ev.meanReward()
    print 'Pulls: '
    print ev.meanNbDraws()
    meanRegret = ev.meanRegret()
    if graphic == 'yes':
        semilogx(1+tsav, meanRegret, color = colors[k])
        xlabel('Time')
        ylabel('Regret')
    k = k+1

if graphic == 'yes':
    legend([policy.__class__.__name__ for policy in policies], loc=0)
    title('Average regret for various policies')
    show()
