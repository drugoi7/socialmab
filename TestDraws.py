# -*- coding: utf-8 -*-
'''This simulation compare the result of the the three different scenarios'''

__author__ = "Cantore Andrea"
__version__ = "$Revision: 1.0 $"


from environment.MAB import MAB
from arm.Bernoulli import Bernoulli
from arm.Poisson import Poisson
from arm.BranchProcess import BranchProcess
from arm.TimedBranchProcess import TimedBranchProcess
from arm.BernoulliSocial import BernoulliSocial
import numpy as nm
import matplotlib.pyplot as plt
import time
import os
from policy import CascadeEvaluation.Thompson
from Evaluation import *
from kullback import *
from posterior.Beta import Beta


colors = ['blue', 'green', 'red', 'cyan', 'magenta', 'black']
graphic = 'yes'
scenario = 0

environment = []

environment.append([Poisson(0.9),Bernoulli(1),Poisson(0.9),Bernoulli(1)])


date = str(int(round(time.time() * 1000)))

filename = 'files/menu'+date+'.txt'
if not os.path.exists(os.path.dirname(filename)):
    os.makedirs(os.path.dirname(filename))
f = open(filename ,'w')

for e in range(0,len(environment)):
    
    print >>f,'Case'+str(e+1)+': '
    print >>f,'PoissonP: '+str(environment[e][0].p)
    print >>f,'BernoulliP: '+str(environment[e][1].p)
    print >>f,'PoissonP2: '+str(environment[e][2].p)
    print >>f,'BernoulliP2: ' + str(environment[e][3].p)
    
lambdas = nm.linspace(0.0000000001,99999.,1000)
values = nm.linspace(9000,10.,1000)
meanRwrd  ={}

for lamb in range(len(lambdas)):
    meanRwrd[lambdas[lamb]] =values[lamb]
    
filename = 'files/draws/SimulationCompareLambdas_'+date+'.png'
if not os.path.exists(os.path.dirname(filename)):
    os.makedirs(os.path.dirname(filename))
f = open(filename ,'w')
    
 
plt.figure(1)
plt.xlabel('lambda')
plt.ylabel('Mean Reward')
plt.title('Rewards for the variation of lambda in Timed Branch Process')
plt.plot(meanRwrd.keys(), meanRwrd.values() ,color = colors[0])
plt.savefig('files/draws/SimulationCompareLambdas_'+date+'.png', bbox_inches='tight')
f.close()
plt.close()