class discrete_cdf:
    def __init__(self,data):
        self._data = data 
        self._data_len = float(len(data))
        
    def __call__(self,point):
        return (sum(i<=point for i in self._data) / 
                self._data_len)