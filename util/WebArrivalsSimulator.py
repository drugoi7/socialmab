# -*- coding: utf-8 -*-
'''A utility class for the simulation of the Web Arrivals.'''

from numpy.random import exponential

__author__ = "Andrea Cantore"
__version__ = "$Revision: 1.0 $"

class WebArrivalsSimulator():
    
    def __init__(self, arrivalRate, limitTime):
        self.poisson_arrivals = exponential(arrivalRate)
        self.webArrival = {}
        t = self.poisson_arrivals
        while t <= limitTime:
            self.webArrival[t] = 1 
            t = t + exponential(arrivalRate)
            
    def getWebArrivals(self):
        return self.webArrival