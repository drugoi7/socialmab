# -*- coding: utf-8 -*-
'''A utility class for the simulation of the Web Arrivals.'''

__author__ = "Andrea Cantore"
__version__ = "$Revision: 1.0 $"

class UpdateSimulator():
    
    def __init__(self, arrivalRate, limitTime):
        self.webArrival = {}
        ar = 1./arrivalRate
        t = ar
        while t <= limitTime:
            self.webArrival[t] = 2 
            t = t + ar
            
    def getUpdateEvents(self):
        return self.webArrival