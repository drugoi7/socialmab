# -*- coding: utf-8 -*-
'''A utility class for managing Poisson Processes.'''
import bisect
import arm


__author__ = "Andrea Cantore"
__version__ = "$Revision: 1.0 $"

class Calendar():
    
        
    def __init__(self,calendar = {}):
        self.calendar = calendar   
        self.countEvent = 0
        self.iteration = 0
        
    def mergeEvents(self,newEvents):
        if len(newEvents) > 0:
            self.calendar.update(newEvents)
        
    def countEventsBetween(self,fr,to):
        if(fr == to and self.countEvent == 0):
            ks = sorted(self.calendar.keys(),key = type(self.calendar.keys()[0]))
            del self.calendar[ks[0]]
            self.countEvent = 0
            return 1
        else:
            if (len(self.calendar) > 0 and self.countEvent < len(self.calendar)):
                ks = sorted(self.calendar.keys(),key = type(self.calendar.keys()[0]))
                reward = ks.index(to) - ks.index(fr) +1
                for i in range(ks.index(to) - ks.index(fr)+1):
                    del self.calendar[ks[i]]
                self.countEvent = 0
                return reward
            else:
                return 0
    
    def nextEvent(self):
        ks = sorted(self.calendar.keys(),key = type(self.calendar.keys()[0]))       
        if(self.countEvent != len(ks)-1):
            self.countEvent = self.countEvent+1
        else:
            self.countEvent = len(ks)-1    
        return ks[self.countEvent]
    
    def nextEventJump(self,jump):
        ks = sorted(self.calendar.keys(),key = type(self.calendar.keys()[0]))
        
        
        #Starting index
        if self.iteration == 0:
            start = 0
            self.iteration = self.iteration +1    
            if(ks[start]) > jump:
                return 0    
        else:
            start = ks[self.countEvent]
            if(start) > jump:
                return 0

        while (self.countEvent < (len(ks)-1) and ks[self.countEvent+1] <= jump):
            self.countEvent = self.countEvent+1

        return ks[self.countEvent]
    
    def lastEvent(self):
        ks = sorted(self.calendar.keys(),key = type(self.calendar.keys()[0]))
        return ks[self.countEvent]
    
    def lastEventSocial(self):
        ks = sorted(self.calendar.keys(),key = type(self.calendar.keys()[0]))
        return ks[0]
    
    def reset(self, calendar = {}):
        self.calendar = calendar   
        self.downEvent = 0
        self.upEvent = 0
        self.countEvent = 0
        
    