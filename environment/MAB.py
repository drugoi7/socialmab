# -*- coding: utf-8 -*-
'''
Environement for a Multi-armed bandit problem 
with arms given in the 'arms' list 
'''

__author__ = "Olivier Cappé,Aurélien Garivier"
__version__ = "$Revision: 1.5 $"

from Result import *
from Environment import Environment

class MAB(Environment):
    """Multi-armed bandit problem with arms given in the 'arms' list"""
    
    def __init__(self, arms, totalEvents = None, noUpdateEvents = False, instantaneusFeed = False):
        self.arms = arms
        self.nbArms = len(arms)
        self.totalEvents = totalEvents.calendar
        self.noUpdateEvents = noUpdateEvents
        self.instantaneusFeed = instantaneusFeed
        # supposed to have a property nbArms

    def play(self, policy, horizon):
        if self.totalEvents is None:
            policy.startGame()
            result = Result(self.nbArms, horizon)
            i = 0
            for t in range(horizon):
                choice = policy.choice()
                reward = self.arms[choice].draw()   
                i = i+1
                policy.getReward(choice, reward)
                probabilities = policy.getProbabilities()
                result.store(t, choice, reward, probabilities)
            return result
        else:
            policy.startGame()
            result = Result(self.nbArms, horizon)
            i = 0
            ks = sorted(self.totalEvents.keys(),key = type(self.totalEvents.keys()[0]))
            for even in ks:
                if self.totalEvents[even] == 2 or self.noUpdateEvents:
                    if self.instantaneusFeed:
                        policy.cascadeEvolution.updateEstimators(ks[-1])
                    else:
                        policy.cascadeEvolution.updateEstimators(even)
                    if self.noUpdateEvents :
                        choice = policy.choice()
                        reward = self.arms[choice].draw()   
                        policy.getReward(choice, reward)
                        probabilities = policy.getProbabilities()
                        result.store(i, choice, reward, probabilities)
                        i = i+1
                else:
                        choice = policy.choice()
                        reward = self.arms[choice].draw()   
                        policy.getReward(choice, reward)
                        probabilities = policy.getProbabilities()
                        result.store(i, choice, reward, probabilities)
                        i = i+1
            return result
    
    def getArmsType(self):
        return self.arms[0].__class__.__name__