# -*- coding: utf-8 -*-
'''
Environement for a Multi-armed bandit problem 
with arms given in the 'arms' list 
'''

__author__ = "Olivier Cappé,Aurélien Garivier"
__version__ = "$Revision: 1.5 $"

from Result import *
from Environment import Environment

class MAB(Environment):
    """Multi-armed bandit problem with arms given in the 'arms' list"""
    
    def __init__(self, arms):
        self.arms = arms
        self.nbArms = len(arms)
        self.numIteration = 0
        self.greedyArm = 0

        # supposed to have a property nbArms

    def play(self, policy, horizon):
        policy.startGame()
        result = Result(self.nbArms, horizon)
        for t in range(horizon):
            if self.numIteration > horizon/25:
                choice = policy.choice()
            else:
                self.numIteration = self.numIteration+1
                self.greedyArm = self.greedyArm+1
                if self.greedyArm <= self.nbArms-1:
                    choice =  self.greedyArm
                else:
                    choice = 0
                    self.greedyArm = 0
            reward = self.arms[choice].draw()   
            policy.getReward(choice, reward)
            probabilities = policy.getProbabilities()
            result.store(t, choice, reward, probabilities)
        return result
    
    def getArmsType(self):
        return self.arms[0].__class__.__name__