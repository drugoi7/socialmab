# -*- coding: utf-8 -*-
'''Demonstration file for the pyBandits package'''

__author__ = "Olivier Cappé, Aurélien Garivier, Emilie Kaufmann"
__version__ = "$Revision: 1.6 $"


from environment.MAB import MAB
from arm.Bernoulli import Bernoulli
from arm.Binomial import Binomial
from arm.Poisson import Poisson
from arm.Exponential import Exponential
from arm.SocialSimulator import BranchProcess
from policy.UCB import UCB
from numpy import *
from matplotlib.pyplot import *
from util.WebArrivalsSimulator import WebArrivalsSimulator
from posterior.Beta import Beta
from calendar.Calendar import Calendar

from policy.UCBV import UCBV
from policy.klUCB import klUCB
from policy.KLempUCB import KLempUCB
from policy.BayesUCB import BayesUCB
from policy.ThompsonBeta import ThompsonBeta
from Evaluation import *
from kullback import *
import copy
import random
from sys import argv
import time

from joblib import Parallel, delayed
import multiprocessing

def parallelFunction(n):

    p1 = round(random.uniform(0.01,0.99),2)
    b1 = round(random.uniform(0,1),2)
    p2 = round(random.uniform(0.01,0.99),2)
    b2 = round(random.uniform(0,1),2)

    # Number of pulls
    wa = WebArrivalsSimulator(1,5000)
    webArrivals = wa.getWebArrivals()
    calWa = Calendar(webArrivals)
    horizon = len(webArrivals)
    

    
    PoissonP = Poisson(p1)
    BernoulliP = Bernoulli(b1)
    PoissonP2 = Poisson(p2)
    BernoulliP2 = Bernoulli(b2)
    population = 500000
    arrivalRate = 0.05
    blacklist=[]
    blacklist2 = []
    
    target = open('Simulations/ComparisonVarMeanBeta/'+str(n+1)+'_'+'Poi('+str(PoissonP.p)+'),Ber('+str(BernoulliP.p)+')'+'Poi2('+str(PoissonP2.p)+'),Ber2('+str(BernoulliP2.p)+').txt','w')    
    target.write( 'WebArrivals: '+ str(len(webArrivals)))
    target.write('\n')
    target.write('Branch Process Parameter: Poisson('+str(p1)+')')
    target.write('\n')
    target.write('Click Process Parameter: Bernoulli('+str(b1)+')')
    target.write('\n')
    target.write('Branch Process Parameter 2: Poisson('+str(p2)+')')
    target.write('\n')
    target.write('Click Process Parameter 2: Bernoulli('+str(b2)+')')
    target.write('\n')
    
    env = []
    env.append(MAB([BranchProcess(BernoulliP,PoissonP,calWa,population,arrivalRate,blacklist),
                BranchProcess(BernoulliP2,PoissonP2,calWa,population,arrivalRate,blacklist)]))
    env.append(MAB([BranchProcess(BernoulliP,PoissonP,calWa,population,arrivalRate,blacklist2),
                BranchProcess(BernoulliP2,PoissonP2,calWa,population,arrivalRate,blacklist2)]))
    
    policies = [ThompsonBeta(env[0].nbArms,Beta,population)]
    
    
    tsav = int_(linspace(100,horizon-1,horizon))
    
    if graphic == 'yes':
        figure(1)
    
    legen_g = []
    k=0
    for policy in policies:
        print policy
        if k > 0:
            env[k] = env[k-1]
            for arm in env[k].arms:
                arm.webArrivals.reset(webArrivals)
        ev = Evaluation(env[k], policy, nbRep, horizon, tsav)
        target.write( 'Mean Reward: ')
        target.write('\n')
        meanRew = ev.meanReward()
        target.write('Mean Reward Arms '+str(meanRew)+'\n')
        target.write('\n')
        target.write( 'Pulls: ')
        target.write('\n')
        meanDraws = ev.meanNbDraws()
        for item in range(len(meanDraws)):
            target.write('Reward Arm'+str(item+1)+' '+str(meanDraws[item])+'\n')
        variances = policy.allV
        means = policy.allC
        if graphic == 'yes':
            for arm in range(len(variances)):        
                semilogx(1+tsav, means[arm], color = colors[arm])
                legen_g.append('Mean Arm '+ str(arm+1))
                semilogx(1+tsav, variances[arm], color = colors[arm+2])
                legen_g.append('Variance Arm '+ str(arm+1))
            xlabel('Horizon')
            ylabel('Variance/Mean')
        k = k+1
    
    if graphic == 'yes':
        legend(legen_g, loc=0)
        title('Arm 1: Poi('+str(PoissonP.p)+'),Ber('+str(BernoulliP.p)+')'+'Arm 2: Poi('+str(PoissonP2.p)+'),Ber('+str(BernoulliP2.p)+')')
        savefig('Simulations/ComparisonVarMeanBeta/'+str(n+1)+'_'+'Poi('+str(PoissonP.p)+'),Ber('+str(BernoulliP.p)+')'+'Poi2('+str(PoissonP2.p)+'),Ber2('+str(BernoulliP2.p)+').png')
        matplotlib.pyplot.close()
    
    target.write('\n')
    target.write('*****************************************************************************')
    target.close()


num_cores = multiprocessing.cpu_count()

ts = int(time.time())
target = open('Simulations/ComparisonVarMeanBeta/menu'+str(ts)+'.txt','w')
target.write('Web Arrival Rate: 1')
target.write('\n')
target.write('Web Arrival Max Time: 5000')
target.write('\n')
target.write('Social Arrival Rate: 0.05')
target.write('\n')
target.write('Social Population: 500000')
target.write('\n')
target.write('********************************************************************************')
target.close()

colors = ['blue', 'green', 'red', 'cyan', 'magenta', 'black']
graphic = 'yes'

# Experient Repetitions
nbRep = 1

Parallel(n_jobs=12)(delayed(parallelFunction)(n) for n in range(200))




