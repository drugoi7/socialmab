# -*- coding: utf-8 -*-
'''The Thompson (Bayesian) index policy.
Reference: [Thompson - Biometrika, 1933].
'''

__author__ = "Olivier Cappé, Aurélien Garivier, Emilie Kaufmann"
__version__ = "$Revision: 1.9 $"


from random import choice

from IndexPolicy import IndexPolicy

class ThompsonBeta(IndexPolicy):
  """The Thompson (Bayesian) index policy.
  Reference: [Thompson - Biometrika, 1933].
  """

  def __init__(self, nbArms, posterior, maxPopulation):
    self.nbArms = nbArms
    self.posterior = dict()
    self.V = [0. for arm in range(self.nbArms)]
    self.largestDiff = [1. for arm in range(self.nbArms)]
    self.I = [[0.,0.,0.] for arm in range(self.nbArms)]
    self.N = [maxPopulation for arm in range(self.nbArms)]
    self.C = [[0.,0.] for arm in range(self.nbArms)]
    self.inf = 0
    self.posterior = [posterior() for arm in range(self.nbArms)]
    self.allV = [ [] for arm in range(self.nbArms)]
    self.allC = [ [] for arm in range(self.nbArms)]
    self.plays = [ 0 for arm in range(self.nbArms)]
          

  def startGame(self):
    self.t = 1;
    for arm in range(self.nbArms):
      self.posterior[arm].reset()

  def getReward(self, armC, reward):
      
      # Update Infections values
    self.inf = self.inf + reward
    for arm in range(self.nbArms):
        tempI = self.I[arm][1]
        self.I[arm][1] = self.I[arm][2]
        if arm == armC:
            self.I[arm][2] = self.I[arm][2] + reward
            self.plays[arm] = self.plays[arm] + 1
        self.I[arm][0] = tempI
        # Update Estimator and Variance
        self.C[arm][0] = self.C[arm][1]
        self.C[arm][1] = self.computeEstimator(arm)
        self.V[arm] = self.computeVariance(arm)
        self.allC[arm].append(self.C[arm][1])
        self.allV[arm].append(self.V[arm])
        # Update Posterior
        self.posterior[arm].update(self.plays[arm],self.C[arm][1])
    self.t += 1
    
  def computeIndex(self, arm):
    return self.posterior[arm].sample()

  def computeVariance(self, arm):
      if self.I[arm][1]-self.I[arm][0] != 0 and self.I[arm][1]-1 != 0 and self.I[arm][2]-1 != 0 and self.inf/self.N[arm] != 1:
          if (self.I[arm][1]-self.I[arm][0]) > self.largestDiff[arm]:
              self.largestDiff[arm] = self.I[arm][1]-self.I[arm][0]
          partOne = 1/(self.I[arm][1]-1)
          partTwo = (self.I[arm][0]-1) * (self.V[arm] + pow(self.C[arm][0]-self.C[arm][1],2))
          partThreeInt = (self.I[arm][2]-self.I[arm][1])/(((self.I[arm][1]-self.I[arm][0])*(1-(self.inf/self.N[arm])))) 
          partThree = (self.I[arm][1]-self.I[arm][0])*(pow(partThreeInt-self.C[arm][1],2))    
          return partOne * (partTwo + partThree)
      else:
          if self.I[arm][1]-1 != 0 and self.I[arm][2]-1 != 0 and self.inf/self.N[arm] != 1:
              partOne = 1/(self.I[arm][1]-1)
              partTwo = (self.I[arm][0]-1) * (self.V[arm] + pow(self.C[arm][0]-self.C[arm][1],2))
              partThreeInt = (self.largestDiff[arm])*(1-(self.inf/self.N[arm]))
              partThree = (self.I[arm][1]-self.I[arm][0])*(pow(partThreeInt-self.C[arm][1],2))    
              return partOne * (partTwo + partThree)
          else:
              return self.V[arm]
      
  def computeEstimator(self, arm):
      if self.I[arm][1] != 0 and self.inf/self.N[arm] != 1:
          return (self.I[arm][2]-self.I[arm][1])/(self.I[arm][1]*(1-(self.inf/self.N[arm])))+self.I[arm][0]*self.C[arm][1]/self.I[arm][1]
      else:
          return self.C[arm][1]