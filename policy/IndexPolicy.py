# -*- coding: utf-8 -*-
'''Generic index policy.'''

__author__ = "Olivier Cappé, Aurélien Garivier"
__version__ = "$Revision: 1.5 $"


from random import choice
import numpy as np

from Policy import *

class IndexPolicy(Policy):
    """Class that implements a generic index policy."""
    probabilities = dict()
    
    

#  def __init__(self):

#  def computeIndex(self, arm):

    def choice(self):
        """In an index policy, choose at random an arm with maximal index."""
        index = dict()
        #prob = dict()
        for arm in range(self.nbArms):
            index[arm] = self.computeIndex(arm)
            self.probabilities[arm] = index[arm]
            #prob[arm] = self.getProbabilities()
        #self.probabilities = dict.values(prob)
        #self.probabilities = prob
        #print index
        maxIndex = max (index.values())
        bestArms = [arm for arm in index.keys() if index[arm] == maxIndex]
        return choice(bestArms)
    
    def setprobabilities(self,probabilitiesT):
        pulls = self.getNbPulls()
        numPulls = len(pulls)
        for k in range(0,numPulls-1):
            pulls[k] = pulls[k]/numPulls
        return pulls
    
    def getProbabilities(self):
        return self.probabilities