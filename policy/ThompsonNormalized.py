# -*- coding: utf-8 -*-
'''The Thompson (Bayesian) index policy.
Reference: [Thompson - Biometrika, 1933].
'''

__author__ = "Olivier Cappé, Aurélien Garivier, Emilie Kaufmann"
__version__ = "$Revision: 1.9 $"


from random import choice

from IndexPolicy import IndexPolicy

class Thompson(IndexPolicy):
  """The Thompson (Bayesian) index policy.
  Reference: [Thompson - Biometrika, 1933].
  """

  def __init__(self, nbArms, posterior, cascadeEvolution, social):
    self.nbArms = nbArms
    self.posterior = dict()
    for arm in range(self.nbArms):
      self.posterior[arm] = posterior()
    self.cascadeEvolution = cascadeEvolution
    self.social = social
    self.name = 'ThompsonNormalized'

  def startGame(self):
    self.t = 1;
    for arm in range(self.nbArms):
      self.posterior[arm].reset()

  def getReward(self, arm, reward):
    if (reward > self.posterior[arm].getMax()):
        for ar in range(self.nbArms):
            self.posterior[ar].normalize(reward)
    if self.social or reward <= 0:
        self.posterior[arm].update(reward)
    else: 
        self.posterior[arm].update(1)            
    self.t += 1
    
  def computeIndex(self, arm):
    index = self.posterior[arm].sample()
    return index

#  def getProbabilities(self):
#      index = dict()
#      for arm in range(0,self.nbArms):
#        index[arm] = self.posterior[arm].getProbabilities()
#       self.probabilities = dict.values(index)
#      return self.probabilities

