# -*- coding: utf-8 -*-
'''The Thompson (Bayesian) index policy.
Reference: [Thompson - Biometrika, 1933].
'''

__author__ = "Olivier Cappé, Aurélien Garivier, Emilie Kaufmann"
__version__ = "$Revision: 1.9 $"


from random import choice

from IndexPolicy import IndexPolicy

class Thompson(IndexPolicy):
  """The Thompson (Bayesian) index policy.
  Reference: [Thompson - Biometrika, 1933].
  """

  def __init__(self, nbArms, posterior, social = True):
    self.nbArms = nbArms
    self.posterior = dict()
    self.probabilites = dict()
    self.social = social
    for arm in range(self.nbArms):
      self.posterior[arm] = posterior()


  def startGame(self):
    self.t = 1;
    for arm in range(self.nbArms):
      self.posterior[arm].reset()

  def getReward(self, arm, reward):
    if (reward <= 0):
        self.posterior[arm].update(0,reward)
    else:
        if self.social:
            self.posterior[arm].update(1,reward)
        else:
            self.posterior[arm].update(1,1)
    self.t += 1
    
  def computeIndex(self, arm):
    self.probabilites[arm] = (self.posterior[arm].sample())
    return self.probabilites[arm]

  def getProbabilities(self):
    return self.probabilites