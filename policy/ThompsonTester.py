from policy import CascadeEvaluation
from arm.SocialSimulator import BranchProcess
from arm.Poisson import Poisson
from arm.Bernoulli import Bernoulli
from util.WebArrivalsSimulator import WebArrivalsSimulator
from posterior.Normal import Normal

wa = WebArrivalsSimulator(0.5,100)
webArrivals = wa.getWebArrivals()
PoissonP = Poisson(0.99)
BernoulliP = Bernoulli(1)
PoissonP2 = Poisson(0.99)
BernoulliP2 = Bernoulli(1)
population = 100000
arrivalRate = 0.2
blacklist=[]

bp = BranchProcess(BernoulliP,PoissonP,webArrivals,population,arrivalRate,blacklist)
bp2 = BranchProcess(BernoulliP,PoissonP,webArrivals,population,arrivalRate,blacklist)
tmp = CascadeEvaluation(1,Normal,population)
tmp.startGame()

for a in range(0,20):
    inf = bp.draw()
    tmp.getReward(0, inf)
    print inf