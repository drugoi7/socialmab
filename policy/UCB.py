# -*- coding: utf-8 -*-
'''The UCB policy for bounded bandits
  Reference: [Auer, Cesa-Bianchi & Fisher - Machine Learning, 2002], with constant
  set (optimally) to 1/2 rather than 2.
  
  Note that UCB is implemented as a special case of klUCB for the divergence 
  corresponding to the Gaussian distributions, see [Garivier & Cappé - COLT, 2011].'''

__author__ = "Olivier Cappé, Aurélien Garivier"
__version__ = "$Revision: 1.11 $"

from IndexPolicy import IndexPolicy
from math import log,sqrt

class UCB(IndexPolicy):
    """The Upper Confidence Bound (UCB) index.
      """
    def __init__(self, nbArms, cascadeEvolution = None):
        
        self.cascadeEvolution = cascadeEvolution
        self.c = 1.
        self.nbArms = nbArms
        self.nbDraws = dict()
        self.cumReward = dict()
        self.probabilites = dict()
        self.name = 'UCB'
        for arm in range(self.nbArms):
            self.probabilites[arm] = []
        
    def startGame(self):
        self.t = 1
        for arm in range(self.nbArms):
            self.nbDraws[arm] = 0
            self.cumReward[arm] = 0.0

    def computeIndex(self, arm):
        if self.nbDraws[arm] == 0:
            self.probabilites[arm] = (float('+infinity'))
            return self.probabilites[arm]
        else:
            self.probabilites[arm] = (self.cumReward[arm] / self.nbDraws[arm]) + sqrt(2 * log(self.t) / self.nbDraws[arm])
            return  self.probabilites[arm]# Could adapt tolerance to the value of self.t

    def getReward(self, arm, reward):
        self.nbDraws[arm] += 1
        self.cumReward[arm] += reward
        self.t += 1
    # Debugging code
    #print "arm " + str(arm) + " receives " + str(reward)
    #print str(self.nbDraws[arm]) + " " + str(self.cumReward[arm])
    def getProbabilities(self):
        return self.probabilites