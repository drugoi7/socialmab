# -*- coding: utf-8 -*-
'''The Thompson (Bayesian) index policy.
Reference: [Thompson - Biometrika, 1933].
'''
from _snack import choice

__author__ = "Olivier Cappé, Aurélien Garivier, Emilie Kaufmann"
__version__ = "$Revision: 1.9 $"


class CascadeEvaluation():
  """The Thompson (Bayesian) index policy.
  Reference: [Thompson - Biometrika, 1933].
  """

  def __init__(self, nbArms, maxPopulation, calendars, webCalendar,expParam):
    self.nbArms = nbArms
    self.calendars = calendars
    self.webCalendar = webCalendar
    self.V = [0. for arm in range(self.nbArms)]
    self.largestDiff = [1. for arm in range(self.nbArms)]
    self.I = [[0.,0.,0.] for arm in range(self.nbArms)]
    self.N = [maxPopulation for arm in range(self.nbArms)]
    self.C = [[0.,0.] for arm in range(self.nbArms)]
    self.inf = 0.
    self.allV = [ [] for arm in range(self.nbArms)]
    self.allC = [ [] for arm in range(self.nbArms)]
    self.plays = [ 0 for arm in range(self.nbArms)]
    self.webArrival = [ 0 for arm in range(self.nbArms)]
    self.allWebArrival = [ [] for arm in range(self.nbArms)]
    self.allI = [ [] for arm in range(self.nbArms)]
    self.allRewards = [ [] for arm in range(self.nbArms)]
    self.allITuple1 = [ [] for arm in range(self.nbArms)]
    self.allITuple2 = [ [] for arm in range(self.nbArms)]
    self.allITuple3 = [ [] for arm in range(self.nbArms)]
    self.allITot = [ ]
    self.jump = 1./expParam
    self.cumulativeReward = [0 for arm in range(self.nbArms)]
          
  def computeEstimators(self, lastEvent, nextEvent, arm):
    reward = self.calendars[arm].countEventsBetween(lastEvent,nextEvent)
    # Update Infections values
    self.inf = self.inf + reward
    #:
    tempI = self.I[arm][1]
    self.I[arm][1] = self.I[arm][2]
    self.I[arm][2] = self.I[arm][2] + reward
    self.plays[arm] = self.plays[arm] + 1
        
    #
    self.allI[arm].append(self.I[arm][2])
    #
    self.I[arm][0] = tempI
    # Update Estimator and Variance
    self.C[arm][0] = self.C[arm][1]
    self.C[arm][1] = self.computeEstimator(arm)
    self.V[arm] = self.computeVariance(arm)
    self.allC[arm].append(self.C[arm][1])
    self.allV[arm].append(self.V[arm])
    self.allITuple1[arm].append(self.I[arm][0])
    self.allITuple2[arm].append(self.I[arm][1])
    self.allITuple3[arm].append(self.I[arm][2])
    self.allRewards[arm].append(reward)
    return reward
        
  def computeEstimatorsNoReward(self, arm):
    reward = 0
    tempI = self.I[arm][1]
    self.I[arm][1] = self.I[arm][2]
    self.I[arm][2] = self.I[arm][2]
    self.plays[arm] = self.plays[arm] + 1
        
    #
    self.allI[arm].append(self.I[arm][2])
    #
    self.I[arm][0] = tempI
    # Update Estimator and Variance
    self.C[arm][0] = self.C[arm][1]
    self.C[arm][1] = self.computeEstimator(arm)
    self.V[arm] = self.computeVariance(arm)
    self.allC[arm].append(self.C[arm][1])
    self.allV[arm].append(self.V[arm])
    self.allITuple1[arm].append(self.I[arm][0])
    self.allITuple2[arm].append(self.I[arm][1])
    self.allITuple3[arm].append(self.I[arm][2])
    self.allRewards[arm].append(reward)
    return reward

  def updateEstimators(self, limit):
    
    for arm in range(self.nbArms):
        if len(self.calendars[arm].calendar) > 0:
            lastEvent = self.calendars[arm].lastEventSocial()
            nextEvent = self.calendars[arm].nextEventJump(limit)
            if (nextEvent != 0):
                self.cumulativeReward[arm] += self.computeEstimators(lastEvent, nextEvent, arm)
            else:
                self.computeEstimatorsNoReward(arm)
        else:
            self.computeEstimatorsNoReward(arm)
        self.allWebArrival[arm].append(self.webArrival[arm])
        self.webArrival[arm] = 0
    self.allITot.append(self.inf)
    
  def computeVariance(self, arm):
      if (self.I[arm][1]-self.I[arm][0]) != 0 and self.I[arm][1]-1 != 0 and self.I[arm][2]-1 != 0 and (self.inf)/self.N[arm] != 1 and self.I[arm][0] - 1 >= 0:
          if (self.I[arm][1]-self.I[arm][0]) > self.largestDiff[arm]:
              self.largestDiff[arm] = self.I[arm][1]-self.I[arm][0]
          partOne = 1./(self.I[arm][1]-1)
          partTwo = (self.I[arm][0]-1) * (self.V[arm] + pow(self.C[arm][0]-self.C[arm][1],2))
          parthThreeInt1 = (self.I[arm][2]-self.I[arm][1])
          parthThreeInt2 = 1-(self.inf/self.N[arm])
          parthThreeInt3 = (self.I[arm][1]-self.I[arm][0])
          partThreeInt = (parthThreeInt1/parthThreeInt3)*(1./parthThreeInt2)
          partThree = (self.I[arm][1]-self.I[arm][0])*(pow(partThreeInt-self.C[arm][1],2))    
          return partOne * (partTwo + partThree)
      else:
          if self.I[arm][1]-1 != 0 and self.I[arm][2]-1 != 0 and (self.inf)/self.N[arm] != 1 and self.I[arm][0] - 1 >= 0:
              partOne = 1./(self.I[arm][1]-1)
              partTwo = (self.I[arm][0]-1) * (self.V[arm] + pow(self.C[arm][0]-self.C[arm][1],2))
              partThreeInt = (self.largestDiff[arm])*(1-((self.inf)/self.N[arm]))
              partThree = (self.I[arm][1]-self.I[arm][0])*(pow(partThreeInt-self.C[arm][1],2))    
              return partOne * (partTwo + partThree)
          else:
              return self.V[arm]
                
  def computeEstimator(self, arm):
      if self.I[arm][1] != 0 and self.inf/self.N[arm] != 1:
          result = (self.I[arm][2]-self.I[arm][1])/((self.I[arm][1])*(1-((self.inf)/self.N[arm])))+self.I[arm][0]*self.C[arm][1]/self.I[arm][1]
          return result
      else:
          return self.C[arm][1]
      
  def addWebArrival(self,arm):
    self.I[arm][2] = self.I[arm][2] + 1
    self.inf = self.inf+1
    self.webArrival[arm] = self.webArrival[arm]+1
      
      
  def getReward(self, arm):
      reward = self.cumulativeReward[arm]
      self.cumulativeReward[arm] = 0
      return reward    
