# -*- coding: utf-8 -*-
'''The Thompson (Bayesian) index policy.
Reference: [Thompson - Biometrika, 1933].
'''

__author__ = "Olivier Cappé, Aurélien Garivier, Emilie Kaufmann"
__version__ = "$Revision: 1.9 $"


from random import choice

from IndexPolicy import IndexPolicy

class Thompson(IndexPolicy):
  """The Thompson (Bayesian) index policy.
  Reference: [Thompson - Biometrika, 1933].
  """

  def __init__(self, nbArms, posterior, cascadeEvolution):
    self.nbArms = nbArms
    self.posterior = dict()
    self.posterior = [posterior() for arm in range(self.nbArms)]
    self.cascadeEvolution = cascadeEvolution
    self.name = 'ThompsonSamplingMod'
          

  def startGame(self):
    self.t = 1;
    for arm in range(self.nbArms):
      self.posterior[arm].reset()

  def getReward(self, choice, reward):   
    for arm in range(self.nbArms):
        self.posterior[arm].update(self.cascadeEvolution.C[arm][1],self.cascadeEvolution.V[arm])
    self.t += 1

  def computeIndex(self, arm):
    return self.posterior[arm].sample()