# -*- coding: utf-8 -*-
'''Demonstration file for the pyBandits package'''

__author__ = "Olivier Cappé, Aurélien Garivier, Emilie Kaufmann"
__version__ = "$Revision: 1.6 $"


from environment.MAB import MAB
from arm.Bernoulli import Bernoulli
from arm.Binomial import Binomial
from arm.Poisson import Poisson
from arm.Exponential import Exponential
from arm.SocialSimulator import BranchProcess as delayedF
from arm.SocialSimulatorInstantaneus import BranchProcess as instantF
from util.UpdateSimulator import UpdateSimulator
from policy.UCB import UCB
from numpy import *
from matplotlib.pyplot import *
from util.WebArrivalsSimulator import WebArrivalsSimulator
from posterior.BetaNorm import Beta
from calendar.Calendar import Calendar

from policy.ThompsonNormalized import Thompson
from Evaluation import *
from kullback import *
import copy
import random
from sys import argv
import csv
import time
from policy.CascadeEvaluation import CascadeEvaluation
import os

from joblib import Parallel, delayed
import multiprocessing

def parallelFunction(variabili,n,graphic,texts):
    
    if texts or graphic:
        if not os.path.exists('Simulations/ComparisonVarMean/'+str(n)):
            os.makedirs('Simulations/ComparisonVarMean/'+str(n))

    p1 = variabili[0]
    b1 = variabili[1]
    p2 = variabili[2]
    b2 = variabili[3]
    
    # Number of pulls
    wa = WebArrivalsSimulator(variabili[6],variabili[4])
    webArrivals = wa.getWebArrivals()
    ks = sorted(webArrivals.keys(),key = type(webArrivals.keys()[0]))
    calWa = Calendar(webArrivals)
    horizon = len(webArrivals)
    
    PoissonP = Poisson(p1)
    BernoulliP = Bernoulli(b1)
    PoissonP2 = Poisson(p2)
    BernoulliP2 = Bernoulli(b2)
    population = variabili[5]
    arrivalRate = variabili[7]
    blacklist=[random.randint(0,population) for x in xrange(horizon)]
    blacklistOriginal = copy.deepcopy(blacklist)
    socialCalendar = Calendar({})
    socialCalendar2 = Calendar({})
          
    # Event Calendar
   
    allEvents = Calendar(webArrivals)

    print 'Current: Poisson('+str(p1)+','+str(p2)+')'+' Bernoulli('+str(b1)+','+str(b2)+')'
    print 'Arrival Rates(S'+str(arrivalRate)+',W'+str(variabili[6])+')'+' Populations('+str(population)+','+str(variabili[4])+')'

    cs = CascadeEvaluation(2,population,[socialCalendar,socialCalendar2],calWa,arrivalRate)
    
    target = open('Simulations/ComparisonVarMean/'+str(n)+'/'+str(n+1)+'_'+'Poi('+str(PoissonP.p)+'),Ber('+str(BernoulliP.p)+')'+'Poi2('+str(PoissonP2.p)+'),Ber2('+str(BernoulliP2.p)+').txt','w')    
    target.write( 'WebArrivals: '+ str(len(webArrivals)))
    target.write('\n')
    target.write('Branch Process Parameter: Poisson('+str(p1)+')')
    target.write('\n')
    target.write('Click Process Parameter: Bernoulli('+str(b1)+')')
    target.write('\n')
    target.write('Branch Process Parameter 2: Poisson('+str(p2)+')')
    target.write('\n')
    target.write('Click Process Parameter 2: Bernoulli('+str(b2)+')')
    target.write('\n')
    target.write('Max Events: '+str(variabili[4]))
    target.write('\n')
    target.write('Max Social Population: '+str(population))
    target.write('\n')
    target.write('Web Arrival Rate: '+str(variabili[6]))
    target.write('\n')
    target.write('Social Arrival rate: '+str(arrivalRate))
    target.write('\n')
    
    env = []
    env.append(MAB([delayedF(BernoulliP,PoissonP,calWa, socialCalendar,population,arrivalRate,cs,0,blacklist),
            delayedF(BernoulliP2,PoissonP2,calWa, socialCalendar2,population,arrivalRate,cs,1,blacklist)],allEvents, True, False))
    policies = [Thompson(env[0].nbArms,Beta,cs,True),Thompson(env[0].nbArms,Beta,cs,True),Thompson(env[0].nbArms,Beta,cs,False)]
    
    envCopy = copy.deepcopy(env)
    
    tsav = int_(linspace(0,horizon-1,horizon))
    
    if graphic == 'yes':
        figure(1)
    
    legen_g = []
    k=0
    for policy in policies:
        if k > 0:
            blacklist = copy.copy(blacklistOriginal)
            env = copy.deepcopy(envCopy)
            env = []
            calWa.countEvent = 0
            env.append(MAB([instantF(BernoulliP,PoissonP,calWa, socialCalendar,population,arrivalRate,cs,0,blacklist),
                            instantF(BernoulliP2,PoissonP2,calWa, socialCalendar2,population,arrivalRate,cs,1,blacklist)],allEvents, True, True))
            env[0].totalEvents = allEvents.calendar
            policy.cascadeEvolution = env[0].arms[0].cascadeEvolution
        ev = Evaluation(env[0], policy, nbRep, horizon, tsav)
        
        if texts:
            target.write('\n'+policy.name+'\n')
            target.write('Mean Reward: ')
            target.write('\n')
            
        meanRew = ev.meanReward()
        
        if texts:
            target.write('Mean Reward Arms '+str(meanRew)+'\n')
            target.write('\n')
            target.write( 'Pulls: ')
            target.write('\n')
        meanDraws = ev.meanNbDraws()
        
        if graphic:
            for item in range(len(meanDraws)):
                target.write('Pulls Arm'+str(item+1)+' '+str(meanDraws[item])+'\n')
            variances = cs.allV
            means = cs.allC
            choices = ev.cumChoices[0]
            for arm in range(len(variances)):        
                tsavVar = int_(linspace(100,len(means[arm])-1,len(means[arm])))     
                semilogx(1+tsavVar, means[arm], color = colors[arm])
                legen_g.append('Mean Arm '+ str(arm+1))       
               #semilogx(1+tsav, variances[arm], color = colors[arm+2])
               #legen_g.append('Variance Arm '+ str(arm+1))
            xlabel('Horizon')
            ylabel('Variance/Mean')
            legend(legen_g, loc=0)
            title('Arm 1: Poi('+str(PoissonP.p)+'),Ber('+str(BernoulliP.p)+')'+'Arm 2: Poi('+str(PoissonP2.p)+'),Ber('+str(BernoulliP2.p)+')')
            savefig('Simulations/ComparisonVarMean/'+str(n)+'/'+str(n+1)+'_'+policy.name+'_'+env[0].arms[0].name+'_'+'Poi('+str(PoissonP.p)+'),Ber('+str(BernoulliP.p)+')'+'Poi2('+str(PoissonP2.p)+'),Ber2('+str(BernoulliP2.p)+').png')
            matplotlib.pyplot.clf()
            scatter(tsav-1,choices,s=[ 0.01 for f in range(len(choices))])
            xlabel('Horizon')
            ylabel('Arm')
            title('Arm 1: Poi('+str(PoissonP.p)+'),Ber('+str(BernoulliP.p)+')'+'Arm 2: Poi('+str(PoissonP2.p)+'),Ber('+str(BernoulliP2.p)+')')
            savefig('Simulations/ComparisonVarMean/'+str(n)+'/'+str(n+1)+'_'+policy.name+'_'+env[0].arms[0].name+'_'+'Choices_Poi('+str(PoissonP.p)+'),Ber('+str(BernoulliP.p)+')'+'Poi2('+str(PoissonP2.p)+'),Ber2('+str(BernoulliP2.p)+').png')
            matplotlib.pyplot.clf()
            percentage = np.asarray(ev.meanProbabilities())
            legen_g = []
            for arm in range(len(env[0].arms)):        
                semilogx(1+tsav, percentage[:,arm], color = colors[arm])
                legen_g.append('Percentage Arm '+ str(arm+1))
            xlabel('Horizon')
            ylabel('Percentage of pulls')
            legend(legen_g, loc=0)
            title('Arm 1: Poi('+str(PoissonP.p)+'),Ber('+str(BernoulliP.p)+')'+'Arm 2: Poi('+str(PoissonP2.p)+'),Ber('+str(BernoulliP2.p)+')')
            savefig('Simulations/ComparisonVarMean/'+str(n)+'/'+str(n+1)+'_'+policy.name+'_'+env[0].arms[0].name+'_'+'Usage_Poi('+str(PoissonP.p)+'),Ber('+str(BernoulliP.p)+')'+'Poi2('+str(PoissonP2.p)+'),Ber2('+str(BernoulliP2.p)+').png')
            matplotlib.pyplot.close()    
        
        if texts:
            target2 = open('Simulations/ComparisonVarMean/'+str(n)+'/'+str(n+1)+'_'+policy.name+'_'+env[0].arms[0].name+'_'+'Result.csv','w') 
            wr = csv.writer(target2,delimiter=',')
            probA = ev.probabilities[0]
            
            if k > 0:
                n = 0
                ks = sorted(allEvents.calendar.keys(),key = type(allEvents.calendar.keys()[0]))
                for even in ks:
                    if allEvents.calendar[even] == 2:
                        probA.insert(n, {0:0,1:0})
                    n = n+1
                        
            probAList = [el[0] for el in probA]
            probBList = [el[1] for el in probA]
            res = [cs.allI[0],cs.allI[1],cs.allRewards[0],cs.allRewards[1],cs.allWebArrival[0],cs.allWebArrival[1],probAList,probBList,cs.allITuple1[0],cs.allITuple2[0],cs.allITuple3[0],cs.allITuple1[1],cs.allITuple2[1],cs.allITuple3[1],cs.allV[0],cs.allV[1],cs.allC[0],cs.allC[1],cs.allITot]
            wr.writerow(['Infetti A','Infetti B','SocialArrival A','SocialArrival B','WebArrival A','WebArrival B','UCB A','UCB B B','I_k-1_a','I_k_a','I_k+1_a','I_k-1_b','I_k_b','I_k+1_b','Varianza a','Varianza b', 'Media a','Media b','Infetti Totali'])
            wr.writerows(zip(*res))
            target2.close()
            target.write('\n')
            target.write('*****************************************************************************')
    
        
        k = k+1
    if texts:
        target.close()


num_cores = multiprocessing.cpu_count()

colors = ['blue', 'green', 'red', 'cyan', 'magenta', 'black']
graphic = 'yes'

# Experient Repetitions
nbRep = 1

#===============================================================================
# population1 = [1000,2000,3000,4000,5000,10000,15000]
# population2 = [5000,10000,15000,30000,100000]
# arrivalRate1 = [0.5,1,3,5,10,20]
# arrivalRate2 = [0.5,1,3,5]
#===============================================================================

population1 = [5000]
population2 = [500000]
arrivalRate1 = [1]
arrivalRate2 = [1]
varRand = []
varRand.append([0.8,1,0.5,1])
#===============================================================================
# varRand.append([0.4,0.5,0.5,0.4])
# varRand.append([0.1,0.9,0.9,0.1])
# varRand.append([0.02,0.05,0.07,0.05])
# varRand.append([0.05,0.1,0.1,0.1])
# varRand.append([0.001,0.9,0.001,0.5])
# varRand.append([0.5,0.5,0.5,0.5])
# varRand.append([0.9,0.5,0.9,0.5])
#===============================================================================

nExp = len(varRand)

menus = 0
variabili = []
for pop1 in population1:
    for pop2 in population2:
        for ar1 in arrivalRate1:
            for ar2 in arrivalRate2:
                ts = int(time.time())
                target = open('Simulations/ComparisonVarMean/menu'+str(menus+1)+'.txt','w')
                target.write('Web Arrival Rate: '+str(ar1))
                target.write('\n')
                target.write('Web Arrival Max Time: '+str(pop1))
                target.write('\n')
                target.write('Social Arrival Rate: '+str(ar2))
                target.write('\n')
                target.write('Social Population: '+str(pop2))
                target.write('\n')
                target.write('********************************************************************************')
                menus = menus+1
                for vr in range(nExp):
                    variabili.append([varRand[vr][0],varRand[vr][1],varRand[vr][2],varRand[vr][3],pop1,pop2,ar1,ar2])
                    target.write('\n')
                    target.write('Bernoulli 1: '+str(varRand[vr][0]))
                    target.write('\n')
                    target.write('Poisson 1: '+str(varRand[vr][1]))
                    target.write('\n')
                    target.write('Bernoulli 2: '+str(varRand[vr][2]))
                    target.write('\n')
                    target.write('Poisson 2: '+str(varRand[vr][3]))
                    target.write('\n')
                    target.write('********************************************************************************')
                target.close()                


Parallel(n_jobs=12)(delayed(parallelFunction)(variabili[n],n,True,True) for n in range(len(variabili)))




