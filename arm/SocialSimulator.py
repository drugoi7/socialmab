# -*- coding: utf-8 -*-
'''Generate a reward from a Branch Process.'''
from _dbus_bindings import String

__author__ = "Andrea Cantore"
__version__ = "$Revision: 1.0 $"

from Arm import Arm
from branchProcess.GaltonWatsonProcess import GaltonWatsonProcess

class BranchProcess(Arm):
    """Banch Process arm."""

    def __init__(self, b, p, webArrivals, socialCalendar , maxPopulation, arrivalRate, cascadeEvolution,arm, blacklist = []):
        self.GaltonProcessDistribution = GaltonWatsonProcess(p, maxPopulation, socialCalendar, arrivalRate, blacklist)
        self.Bernoulli = b
        """ QUALE e' l'expectation """
        self.expectation = float(b.getProbability()) / (1. -  float(p.getProbability()))
        self.webArrivals = webArrivals
        self.socialCalendar = socialCalendar
        self.maxPopulation = maxPopulation
        self.cascadeEvolution = cascadeEvolution
        self.arm = arm
        self.name = 'DelayedBranch'
    
    def draw(self):

        var = self.Bernoulli.draw()
        nextEvent = self.webArrivals.nextEvent();  
        if var == 1:
            self.GaltonProcessDistribution.simulateTree(nextEvent)
            self.socialCalendar.mergeEvents(self.GaltonProcessDistribution.getBranchProcess()) 
            ks = sorted(self.socialCalendar.calendar,key = float)
            #print self.arm,':'
            #print ks  
        self.cascadeEvolution.addWebArrival(self.arm)
        var += self.cascadeEvolution.getReward(self.arm)  
        return var
    
    def getExpectation(self):
        return  self.expectation

    def getProbability(self):
        return self.p
    
    def resetBlackList(self):
        self.GaltonProcessDistribution.blackList = []