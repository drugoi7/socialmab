# -*- coding: utf-8 -*-
'''Generate a reward from a Branch Process.'''

__author__ = "Andrea Cantore"
__version__ = "$Revision: 1.0 $"

from Arm import Arm
from branchProcess.GaltonWatsonProcess import GaltonWatsonProcess

class BranchProcess(Arm):
    """Banch Process arm."""

    def __init__(self, b, p):
        self.GaltonProcessDistribution = p
        self.Bernoulli = b
        """ QUALE e' l'expectation """
        self.expectation = float(b.getProbability()) / (1. -  float(p.getProbability()) )
        
    def draw(self):
        var = self.Bernoulli.draw()
        if(var == 1):
            gws = GaltonWatsonProcess(self.GaltonProcessDistribution)
            var = gws.simulateTree(0)
        return var
    
    def getExpectation(self):
        return  self.expectation

    def getProbability(self):
        return self.p