# -*- coding: utf-8 -*-
'''A utility class for testing the Barch Process simulator.'''


__author__ = "Andrea Cantore"
__version__ = "$Revision: 1.0 $"

from calendar.Calendar import Calendar
from arm.Poisson import Poisson
from arm.Bernoulli import Bernoulli
from arm.SocialSimulator import BranchProcess
from policy.CascadeEvaluation import CascadeEvaluation

webArrivals = Calendar({0.1: 1,0.22: 1,0.45: 1,0.98: 1,1.22: 1,7.36: 1,8.65: 1,8.65: 1,8.65: 1,8.65: 1,8.65: 1,8.65: 1,.965: 1,14.5: 1,23.65: 1,14.65: 1,16.65: 1,12.65: 1,32.5: 1,21.65: 1,6.65: 1,7.65: 1,1.65: 1,20.65: 1,12.75: 1,14.74: 1,1.15: 1,12.45: 1,10.32: 1})
PoissonP = Poisson(0.9)
BernoulliP = Bernoulli(1)
PoissonP2 = Poisson(0.1)
BernoulliP2 = Bernoulli(0.1)
population = 10
arrivalRate = 2
blacklist = []
socialCalendar = Calendar({})
socialCalendar2 = Calendar({})
cs = CascadeEvaluation(2,population,[socialCalendar,socialCalendar2],webArrivals,arrivalRate)
ss = BranchProcess(BernoulliP,PoissonP,webArrivals,socialCalendar,population,arrivalRate,cs,0,blacklist)
ss2 = BranchProcess(BernoulliP2,PoissonP2,webArrivals,socialCalendar2,population,arrivalRate,cs,1,blacklist)

for i in range(0,23):
    print 'Draw one:', ss.draw()
    print ss.GaltonProcessDistribution.blackList
    print 'Draw two:',ss2.draw()
    print ss2.GaltonProcessDistribution.blackList