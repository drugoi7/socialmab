# -*- coding: utf-8 -*-
'''Bernoulli distributed arm.'''

__author__ = "Olivier Cappé, Aurélien Garivier, Andrea Cantore"
__version__ = "$Revision: 1.7 $"

from random import random
from Arm import Arm

class Bernoulli(Arm):
    """Bernoulli distributed arm."""

    def __init__(self, p):
        self.p = p
        self.expectation = p
        
    def draw(self):
        var = float(random()<self.p)
        return var
    
    def getExpectation(self):
        return self.expectation

    def getProbability(self):
        return self.p