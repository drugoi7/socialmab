# -*- coding: utf-8 -*-
'''Geometric distributed arm.'''

__author__ = "Andrea Cantore"
__version__ = "$Revision: 1.0 $"

import numpy as np
from Arm import Arm

class Geometric(Arm):
    """Geometric distributed arm."""
    def __init__(self, p ):
        self.p = p
        
    def draw(self):
        return np.random.geometric(self.p,1)
    
    def getExpectation(self):
        return  self.expectation
    
    def getProbability(self):
        return  self.p