# -*- coding: utf-8 -*-
'''Binomial distributed arm.'''

__author__ = "Andrea Cantore"
__version__ = "$Revision: 1.0 $"

from numpy.random import binomial
from Arm import Arm

class Binomial(Arm):
    """Binomial distributed arm."""

    def __init__(self, maxN, p):
        self.p = p
        self.expectation = p
        self.max = maxN
        
    def draw(self):
        return binomial(self.max,self.p)

    def updateP(self,p):
        self.p = p
        
    def getExpectation(self):
        return self.expectation
    
    def getProbability(self):
        return self.p