# -*- coding: utf-8 -*-
'''Demonstration file for the pyBandits package'''

__author__ = "Olivier Cappé, Aurélien Garivier, Emilie Kaufmann"
__version__ = "$Revision: 1.6 $"


from environment.MAB import MAB
from arm.Bernoulli import Bernoulli
from arm.Binomial import Binomial
from arm.Poisson import Poisson
from arm.Exponential import Exponential
from arm.BranchProcess import BranchProcess
from policy.UCB import UCB
from numpy import *
from matplotlib.pyplot import *

from policy.UCBV import UCBV
from policy.klUCB import klUCB
from policy.KLempUCB import KLempUCB
from policy.BayesUCB import BayesUCB
from Evaluation import *
from kullback import *

colors = ['blue', 'green', 'red', 'cyan', 'magenta', 'black']
graphic = 'yes'
scenario = 0

# Experient Repetitions
nbRep = 10

# Number of pulls

horizon = 2000

if scenario == 0:
    # First scenario (default): Bernoulli experiment with ten arms
    # (figure 2 in [Garivier & Cappé, COLT 2011])
    PoissonP = Poisson(0.01)
    BernoulliP = Bernoulli(0.01)
    PoissonP2 = Poisson(0.9)
    BernoulliP2 = Bernoulli(0.9)
    distr = [ PoissonP, PoissonP2]
    #print distr
    env = MAB([TimedBranchProcess(BernoulliP,PoissonP),TimedBranchProcess(BernoulliP2,PoissonP2)])
    #policies = [UCB(env.nbArms), klUCB(env.nbArms), BayesUCB(env.nbArms,Beta), Thompson(env.nbArms,Beta)]
    policies = [Thompson(env.nbArms,Beta)]


tsav = int_(linspace(100,horizon-1,2000))

if graphic == 'yes':
    figure(1)

k=0
for policy in policies:
    for pol in policies:
        print pol.choice
    ev = Evaluation(env, policy, nbRep, horizon, tsav)
    print 'Mean Reward: '
    print ev.meanReward()
    print 'Pulls: '
    print ev.meanNbDraws()
    meanRegret = ev.meanRegret()
    if graphic == 'yes':
        semilogx(1+tsav, meanRegret, color = colors[k])
        xlabel('Time')
        ylabel('Regret')
    k = k+1

if graphic == 'yes':
    legend([policy.__class__.__name__ for policy in policies], loc=0)
    title('Average regret for various policies')
    show()
