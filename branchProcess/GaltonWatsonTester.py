from arm.Poisson import Poisson
from GaltonWatsonProcess import GaltonWatsonProcess
from calendar.Calendar import Calendar

p = Poisson(0.95)
gws = GaltonWatsonProcess(p,20,Calendar())
for i in range(30):

    gws.simulateTree(0)
    a = gws.getBranchProcess()
    print a
    print gws.getNumberOfChildren()
    gws.reset()