# -*- coding: utf-8 -*-
'''A utility class for evaluating the simulated generations of a branching process.'''

import sys
from numpy.random import exponential
import random as rn

__author__ = "Andrea Cantore"
__version__ = "$Revision: 1.0 $"

class GaltonWatsonProcess():
    
    def __init__(self, distribution, maxPopulation, calendar, arrivalRate = 0.05, blackList = [], trunc = sys.maxint ):
        self.distribution = distribution
        self.trunc = trunc
        self.N = 0
        self.iter = 0
        self.poisson_arrivals = arrivalRate
        self.branchProcess = calendar.calendar
        self.maxPopulation = maxPopulation
        self.blackList = blackList
        
    def simulateTree(self,time):
        child = self.distribution.draw()       
        self.N = child 
        if len(self.blackList) > self.maxPopulation:
            True
        if self.iter != self.trunc :
            for i in range(0,child):
                new_time = float(time+exponential(self.poisson_arrivals))
                labelChild = rn.randint(1,self.maxPopulation)
                if labelChild not in self.blackList:
                    self.blackList.append(labelChild)
                    self.branchProcess[new_time] = labelChild
                self.simulateTree(new_time)
    
    def getNumberOfChildren(self):
        res = self.N
        self.N = 0
        return res
    
    def setPoissonParam(self, arrivalRate):
        self.poisson_arrivals = arrivalRate

    def getBranchProcess(self):
        return self.branchProcess
    
    def setBlackList(self,blackList):
        self.blackList = blackList
        
    def reset(self):
        self.branchProcess = {}
        self.blackList = []
        self.N = 0
        self.iter = 0
        
    def empty(self):
        self.branchProcess = {}