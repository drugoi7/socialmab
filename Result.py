# -*- coding: utf-8 -*-
'''Utility class for handling the results of a Multi-armed Bandits experiment.'''

__author__ = "Olivier Cappé, Aurélien Garivier"
__version__ = "$Revision: 1.7 $"


import numpy as np

class Result:
    """The Result class for analyzing the output of bandit experiments."""
    def __init__(self, nbArms, horizon ):
        self.nbArms = nbArms
        self.choices = [0 for i in range(horizon)]
        self.rewards = np.zeros(horizon)
        self.count = [0. for arm in range(self.nbArms)]
        self.lastT = 0
        self.probabilities = [[] for i in range(horizon)]
    
    def store(self, t, choice, reward, probabilities):
        self.lastT = t
        self.choices[t] = choice
        self.rewards[t] = reward
        #print probabilities
        self.probabilities[t] = probabilities.copy()
    
    def getNbPulls(self):
        if (self.nbArms==float('inf')):
            self.nbPulls=np.array([])
            pass
        else :
            nbPulls = np.zeros(self.nbArms)
            for choice in self.choices:
                nbPulls[choice] += 1
            return nbPulls
    
    def getRegret(self, bestExpectation):
        return np.cumsum(bestExpectation-self.rewards)
    
    def getLastProbability(self):
        return self.probabilites[-1]
    
    def setProbability(self):
        result = self.count[:]
        for i in range(0,len(result)):
            result[i] = result[i]/(self.lastT+1)
        return result
    
    def getProbability(self):
        return self.probabilities
    
    def getChoices(self):
        return self.choices