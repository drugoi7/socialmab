# -*- coding: utf-8 -*-
'''Demonstration file for the pyBandits package'''

__author__ = "Olivier Cappé, Aurélien Garivier, Emilie Kaufmann"
__version__ = "$Revision: 1.6 $"


from environment.MAB import MAB
from arm.Bernoulli import Bernoulli
from arm.Binomial import Binomial
from arm.Poisson import Poisson
from arm.Exponential import Exponential
from arm.SocialSimulator import BranchProcess
from policy.UCB import UCB
from numpy import *
from matplotlib.pyplot import *
from util.WebArrivalsSimulator import WebArrivalsSimulator
from posterior.Normal import Normal
from calendar.Calendar import Calendar

from policy.UCBV import UCBV
from policy.klUCB import klUCB
from policy.KLempUCB import KLempUCB
from policy.BayesUCB import BayesUCB
from policy.Thompson import Thompson
from Evaluation import *
from kullback import *
import copy

colors = ['blue', 'green', 'red', 'cyan', 'magenta', 'black']
graphic = 'yes'

# Experient Repetitions
nbRep = 1

# Number of pulls
wa = WebArrivalsSimulator(3,9000)
webArrivals = wa.getWebArrivals()
calWa = Calendar(webArrivals)
horizon = len(webArrivals)

print 'WebArrivals: ',len(webArrivals)

PoissonP = Poisson(0.9)
BernoulliP = Bernoulli(1)
PoissonP2 = Poisson(0.5)
BernoulliP2 = Bernoulli(1)
population = 500
arrivalRate = 0.5
blacklist=[]
blacklist2 = []

env = []
env.append(MAB([BranchProcess(BernoulliP,PoissonP,calWa,population,arrivalRate,blacklist),
            BranchProcess(BernoulliP2,PoissonP2,calWa,population,arrivalRate,blacklist)]))
env.append(MAB([BranchProcess(BernoulliP,PoissonP,calWa,population,arrivalRate,blacklist2),
            BranchProcess(BernoulliP2,PoissonP2,calWa,population,arrivalRate,blacklist2)]))

policies = [Thompson(env[0].nbArms,Normal,population)]


tsav = int_(linspace(100,horizon-1,horizon))
legen_g = []
if graphic == 'yes':
    figure(1)

k=0
n = 1
for policy in policies:
        print policy
        if k > 0:
            env[k] = env[k-1]
            for arm in env[k].arms:
                arm.webArrivals.reset(webArrivals)
        ev = Evaluation(env[k], policy, nbRep, horizon, tsav)
        print ( 'Mean Reward: ')
        print ('\n')
        meanRew = ev.meanReward()
        print ('Mean Reward Arms '+str(meanRew)+'\n')
        print ('\n')
        print ( 'Pulls: ')
        print ('\n')
        meanDraws = ev.meanNbDraws()
        for item in range(len(meanDraws)):
            print ('Pulls Arm'+str(item+1)+' '+str(meanDraws[item])+'\n')
        variances = policy.allV
        means = policy.allC
        choices = ev.cumChoices[0]
        for arm in range(len(variances)):        
            semilogx(1+tsav, means[arm], color = colors[arm])
            legen_g.append('Mean Arm '+ str(arm+1))
            semilogx(1+tsav, variances[arm], color = colors[arm+2])
            legen_g.append('Variance Arm '+ str(arm+1))
        xlabel('Horizon')
        ylabel('Variance/Mean')
        legend(legen_g, loc=0)
        title('Arm 1: Poi('+str(PoissonP.p)+'),Ber('+str(BernoulliP.p)+')'+'Arm 2: Poi('+str(PoissonP2.p)+'),Ber('+str(BernoulliP2.p)+')')
        savefig('Simulations/ComparisonVarMean/'+str(n+1)+'_'+'Poi('+str(PoissonP.p)+'),Ber('+str(BernoulliP.p)+')'+'Poi2('+str(PoissonP2.p)+'),Ber2('+str(BernoulliP2.p)+').png')
        matplotlib.pyplot.clf()
        scatter(tsav-1,choices,s=[ 0.01 for f in range(len(choices))])
        xlabel('Horizon')
        ylabel('Arm')
        title('Arm 1: Poi('+str(PoissonP.p)+'),Ber('+str(BernoulliP.p)+')'+'Arm 2: Poi('+str(PoissonP2.p)+'),Ber('+str(BernoulliP2.p)+')')
        savefig('Simulations/ComparisonVarMean/'+str(n+1)+'_'+'Choices_Poi('+str(PoissonP.p)+'),Ber('+str(BernoulliP.p)+')'+'Poi2('+str(PoissonP2.p)+'),Ber2('+str(BernoulliP2.p)+').png')
        matplotlib.pyplot.clf()
        percentage = np.asarray(ev.meanProbabilities())
        legen_g = []
        for arm in range(len(env[k].arms)):        
            semilogx(1+tsav, percentage[:,arm], color = colors[arm])
            legen_g.append('Percentage Arm '+ str(arm+1))
        xlabel('Horizon')
        ylabel('Percentage of pulls')
        legend(legen_g, loc=0)
        title('Arm 1: Poi('+str(PoissonP.p)+'),Ber('+str(BernoulliP.p)+')'+'Arm 2: Poi('+str(PoissonP2.p)+'),Ber('+str(BernoulliP2.p)+')')
        savefig('Simulations/ComparisonVarMean/'+str(n+1)+'_'+'Usage_Poi('+str(PoissonP.p)+'),Ber('+str(BernoulliP.p)+')'+'Poi2('+str(PoissonP2.p)+'),Ber2('+str(BernoulliP2.p)+').png')
        matplotlib.pyplot.close()    
        k = k+1
