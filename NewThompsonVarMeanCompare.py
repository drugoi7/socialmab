# -*- coding: utf-8 -*-
'''Demonstration file for the pyBandits package'''

__author__ = "Olivier Cappé, Aurélien Garivier, Emilie Kaufmann"
__version__ = "$Revision: 1.6 $"


from environment.MAB import MAB
from arm.Bernoulli import Bernoulli
from arm.Binomial import Binomial
from arm.Poisson import Poisson
from arm.Exponential import Exponential
from arm.SocialSimulator import BranchProcess
from policy.UCB import UCB
from numpy import *
from matplotlib.pyplot import *
from util.WebArrivalsSimulator import WebArrivalsSimulator
from posterior.Normal import Normal
from calendar.Calendar import Calendar

from policy.UCBV import UCBV
from policy.klUCB import klUCB
from policy.KLempUCB import KLempUCB
from policy.BayesUCB import BayesUCB
from policy.Thompson import Thompson
from Evaluation import *
from kullback import *
import copy
import random
from sys import argv
import time

from joblib import Parallel, delayed
import multiprocessing

def parallelFunction(variabili,n):

    p1 = variabili[0]
    b1 = variabili[1]
    p2 = variabili[2]
    b2 = variabili[3]

    # Number of pulls
    wa = WebArrivalsSimulator(variabili[6],variabili[4])
    webArrivals = wa.getWebArrivals()
    calWa = Calendar(webArrivals)
    horizon = len(webArrivals)
    

    
    PoissonP = Poisson(p1)
    BernoulliP = Bernoulli(b1)
    PoissonP2 = Poisson(p2)
    BernoulliP2 = Bernoulli(b2)
    population = variabili[5]
    arrivalRate = variabili[7]
    blacklist=[]
    blacklist2 = []
    
    target = open('Simulations/ComparisonVarMean/'+str(n+1)+'_'+'Poi('+str(PoissonP.p)+'),Ber('+str(BernoulliP.p)+')'+'Poi2('+str(PoissonP2.p)+'),Ber2('+str(BernoulliP2.p)+').txt','w')    
    target.write( 'WebArrivals: '+ str(len(webArrivals)))
    target.write('\n')
    target.write('Branch Process Parameter: Poisson('+str(p1)+')')
    target.write('\n')
    target.write('Click Process Parameter: Bernoulli('+str(b1)+')')
    target.write('\n')
    target.write('Branch Process Parameter 2: Poisson('+str(p2)+')')
    target.write('\n')
    target.write('Click Process Parameter 2: Bernoulli('+str(b2)+')')
    target.write('\n')
    
    env = []
    env.append(MAB([BranchProcess(BernoulliP,PoissonP,calWa,population,arrivalRate,blacklist),
                BranchProcess(BernoulliP2,PoissonP2,calWa,population,arrivalRate,blacklist)]))
    env.append(MAB([BranchProcess(BernoulliP,PoissonP,calWa,population,arrivalRate,blacklist2),
                BranchProcess(BernoulliP2,PoissonP2,calWa,population,arrivalRate,blacklist2)]))
    
    policies = [Thompson(env[0].nbArms,Normal,population)]
    
    
    tsav = int_(linspace(0,horizon-1,horizon))
    
    if graphic == 'yes':
        figure(1)
    
    legen_g = []
    k=0
    for policy in policies:
        print policy
        if k > 0:
            env[k] = env[k-1]
            for arm in env[k].arms:
                arm.webArrivals.reset(webArrivals)
        ev = Evaluation(env[k], policy, nbRep, horizon, tsav)
        target.write( 'Mean Reward: ')
        target.write('\n')
        meanRew = ev.meanReward()
        target.write('Mean Reward Arms '+str(meanRew)+'\n')
        target.write('\n')
        target.write( 'Pulls: ')
        target.write('\n')
        meanDraws = ev.meanNbDraws()
        for item in range(len(meanDraws)):
            target.write('Pulls Arm'+str(item+1)+' '+str(meanDraws[item])+'\n')
        variances = policy.allV
        means = policy.allC
        choices = ev.cumChoices[0]
        for arm in range(len(variances)):        
            semilogx(1+tsav, means[arm], color = colors[arm])
            legen_g.append('Mean Arm '+ str(arm+1))
            semilogx(1+tsav, variances[arm], color = colors[arm+2])
            legen_g.append('Variance Arm '+ str(arm+1))
        xlabel('Horizon')
        ylabel('Variance/Mean')
        legend(legen_g, loc=0)
        title('Arm 1: Poi('+str(PoissonP.p)+'),Ber('+str(BernoulliP.p)+')'+'Arm 2: Poi('+str(PoissonP2.p)+'),Ber('+str(BernoulliP2.p)+')')
        savefig('Simulations/ComparisonVarMean/'+str(n+1)+'_'+'Poi('+str(PoissonP.p)+'),Ber('+str(BernoulliP.p)+')'+'Poi2('+str(PoissonP2.p)+'),Ber2('+str(BernoulliP2.p)+').png')
        matplotlib.pyplot.clf()
        scatter(tsav-1,choices,s=[ 0.01 for f in range(len(choices))])
        xlabel('Horizon')
        ylabel('Arm')
        title('Arm 1: Poi('+str(PoissonP.p)+'),Ber('+str(BernoulliP.p)+')'+'Arm 2: Poi('+str(PoissonP2.p)+'),Ber('+str(BernoulliP2.p)+')')
        savefig('Simulations/ComparisonVarMean/'+str(n+1)+'_'+'Choices_Poi('+str(PoissonP.p)+'),Ber('+str(BernoulliP.p)+')'+'Poi2('+str(PoissonP2.p)+'),Ber2('+str(BernoulliP2.p)+').png')
        matplotlib.pyplot.clf()
        percentage = np.asarray(ev.meanProbabilities())
        legen_g = []
        for arm in range(len(env[k].arms)):        
            semilogx(1+tsav, percentage[:,arm], color = colors[arm])
            legen_g.append('Percentage Arm '+ str(arm+1))
        xlabel('Horizon')
        ylabel('Percentage of pulls')
        legend(legen_g, loc=0)
        title('Arm 1: Poi('+str(PoissonP.p)+'),Ber('+str(BernoulliP.p)+')'+'Arm 2: Poi('+str(PoissonP2.p)+'),Ber('+str(BernoulliP2.p)+')')
        savefig('Simulations/ComparisonVarMean/'+str(n+1)+'_'+'Usage_Poi('+str(PoissonP.p)+'),Ber('+str(BernoulliP.p)+')'+'Poi2('+str(PoissonP2.p)+'),Ber2('+str(BernoulliP2.p)+').png')
        matplotlib.pyplot.close()    
        k = k+1
    

    
    target.write('\n')
    target.write('*****************************************************************************')
    target.close()


num_cores = multiprocessing.cpu_count()

colors = ['blue', 'green', 'red', 'cyan', 'magenta', 'black']
graphic = 'yes'

# Experient Repetitions
nbRep = 1
nExp = 5
#population1 = [1000,2000,3000,4000,5000,10000,15000]
#population2 = [100,500,1000,3000,5000,10000,15000,30000,100000]
#arrivalRate1 = [0.0001,0.005,0.05,0.5,1,3,5,10,20]
#arrivalRate2 = [0.0001,0.005,0.05,0.5,1,3,5]

population1 = [1000,2000,3000,4000]
population2 = [100,500,10000,15000,100000]
arrivalRate1 = [0.5,1,3,10]
arrivalRate2 = [0.0001,0.05,0.5,1,5]
varRand = []
for i in range(nExp):
    p1 = round(random.uniform(0.01,0.99),2)
    b1 = round(random.uniform(0,1),2)
    p2 = round(random.uniform(0.01,0.99),2)
    b2 = round(random.uniform(0,1),2)
    varRand.append([p1,b1,p2,b2])

menus = 0
variabili = []
for pop1 in population1:
    for pop2 in population2:
        for ar1 in arrivalRate1:
            for ar2 in arrivalRate2:
                ts = int(time.time())
                target = open('Simulations/ComparisonVarMean/menu'+str(menus+1)+'.txt','w')
                target.write('Web Arrival Rate: '+str(ar1))
                target.write('\n')
                target.write('Web Arrival Max Time: '+str(pop1))
                target.write('\n')
                target.write('Social Arrival Rate: '+str(ar2))
                target.write('\n')
                target.write('Social Population: '+str(pop2))
                target.write('\n')
                target.write('********************************************************************************')
                menus = menus+1
                for vr in range(nExp):
                    variabili.append([varRand[vr][0],varRand[vr][1],varRand[vr][2],varRand[vr][3],pop1,pop2,ar1,ar2])
                    target.write('\n')
                    target.write('Bernoulli 1: '+str(varRand[vr][0]))
                    target.write('\n')
                    target.write('Poisson 1: '+str(varRand[vr][1]))
                    target.write('\n')
                    target.write('Bernoulli 2: '+str(varRand[vr][2]))
                    target.write('\n')
                    target.write('Poisson 2: '+str(varRand[vr][3]))
                    target.write('\n')
                    target.write('********************************************************************************')
                target.close()                


Parallel(n_jobs=12)(delayed(parallelFunction)(variabili[n],n) for n in range(len(variabili)))




