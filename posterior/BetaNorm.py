# -*- coding: utf-8 -*-
'''Beta posterior for binary bandits'''

__author__ = "Olivier Cappé, Aurélien Garivier, Emilie Kaufmann"
__version__ = "$Revision: 1.5 $"

from Posterior import Posterior
from arm.Arm import Arm
import copy

from random import betavariate
from scipy.special import btdtri

class Beta(Posterior,Arm):
    """Manipulate posteriors of Bernoulli/Beta experiments.
    """
    def __init__(self, a=1., b=1.):
        self.a = a
        self.b = b
        self.max = 1
        self.N = 0
        self.elements = []

        
    # a number of times that a particular arm yielded a reward
	# b number of times that a particular arm not yielded a reward
    def reset(self, a=0, b=0):
        if a==0:
            a = self.a
        if b==0:
            b = self.b
        self.N = [a, b]
        self.max = 1
        self.S = 1

    def update(self, value):       
        self.N[0] = self.N[0] + value/self.max
        self.N[1] = self.N[1] + 1 - value/self.max
        self.S += 1 
        self.elements.append(copy.copy( self.N))
        
    def normalize(self, newMax):
       self.N[0] = (self.N[0] * self.max) / newMax
       self.N[1] = self.S - self.N[0]
       self.max = newMax
        
    def getMax(self):
        return self.max    
    
	# Beta distribution. Conditions on the parameters are alpha > 0 and beta > 0. Returned values range between 0 and 1.
    def sample(self):
        return betavariate(self.N[0], self.N[1])

	# p-th quantile of the beta distribution.
	# Quantile function specifies, for a given probability in the probability distribution of a random variable, 
	# the value at which the probability of the random variable will be less than or equal to that probability
    def quantile(self, p):
        return btdtri(self.N[0], self.N[1], p) # Bug: do not call btdtri with (0.5,0.5,0.5) in scipy < 0.9

    def draw(self):
        return self.sample()
    
    def getProbabilities(self):
        return self.probabilities
    