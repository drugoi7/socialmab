# -*- coding: utf-8 -*-
'''Beta posterior for binary bandits'''

__author__ = "Olivier CappÃ©, AurÃ©lien Garivier, Emilie Kaufmann"
__version__ = "$Revision: 1.5 $"

from Posterior import Posterior

from numpy.random import normal

class Normal(Posterior):
    """Manipulate posteriors of Bernoulli/Beta experiments.
    """
    def __init__(self, loc=0, scale=0):
        self.loc = loc
        self.scale = scale
        
    def reset(self, loc=0, scale=0):
        self.loc = loc
        self.scale = scale
        
    def sample(self):
        if self.scale == 0:
            return self.loc
        else:
            value = normal(self.loc, self.scale)
            return value
    
    def update(self, mean, variance):
        self.loc = mean
        if variance >= 0:
            self.scale = variance
            


